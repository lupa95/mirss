package com.cesur;
import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.util.Scanner;


public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="MiRSSJoaquin";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    /** 
     * @param args
     */
    public static void main( String[] args )
    {
        //Empieza la aventura
        CrearBBDD();
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        Scanner sc=new Scanner(System.in);
        int opcion;
        do {																					
            System.out.println("|ººººººººººººººººººººººººººººººººººººººººººººº|");				
            System.out.println("|Que desea hacer:                             |");				
            System.out.println("|1. Leer RSS                                  |");
            System.out.println("|2. Mostrar el titulo de un post              |");
            System.out.println("|3. Mostrar todos los posts                   |");
            System.out.println("|4. Buscar post por ID      	              |");
            System.out.println("|5. Buscar por Categoria                      |");
            System.out.println("|6. Buscar por titulo o contenido             |");
            System.out.println("|7. Mostrar create de Trigger                 |");
            System.out.println("|8. Salir                                     |");
            System.out.println("|_____________________________________________|");
        
            opcion = sc.nextInt();
            switch (opcion) {
            case 1:
                leerRSS(b);
                break;
        
            case 2:
                mostrarUnPost(b);
                break;
        
            case 3:
                mostrarTodosLosPost(b);
                break;
        
            case 4:
                
                mostrarPosrtID(b);
                break;
        
            case 5:
                mostrarPorCategoria(b);
                break;
            case 6:
                
                mostrarTituloContenido(b);
                break;
            case 7:
            System.out.println("create trigger asignarCat \non Post \nfor update, insert \nas  \nbegin \n\t declare @catId as int \n\tset @catId = (select id from Categoria) \n\tif @catId= 'null' \n\tbegin \n\t\tset @catId=1 \n\t\tupdate Post set categoria_id=@catId \n\tend \nend");

                break;
               
            }
        } while (opcion != 8);
        
       
    }
    

public static void leerRSS(bbdd b){
    XmlParse x =new XmlParse();
        String url ="https://lenguajedemarcasybbdd.wordpress.com/feed/";
        List<String>  titulos = x.leer("//item/title/text()",url);
        List <String> contenido=x.leer("//item/description/text()",url);
        List <String> categorias= x.leer("//item/category/text()", url);
        List <String> comentarios= x.leer("//item/comments/text()",url);
        List <String> usuario= x.leer("//item/creator/text()",url);
        List <String> fechasPost= x.leer("//item/pubDate/text()", url);
        List <String> fechasComentario= x.leer("//item/slash:comments/text()",url);
       
        
        
        for (int i=0;i<titulos.size();i++){
            System.out.println(i);
            String insertar= "insert into Post (titulo,fechaCreacion,contenido,categoria_id,idUsuario) values('"+titulos.get(i)+"','"+fechasPost.get(i)+"','"+contenido.get(i)+"',(select top(1) id from categoria where nombre='"+categorias.get(i)+"'),(select top(1) id from Usuario where usuario='"+usuario.get(i)+"'))";
            String insertarCom="Insert into Comentario (comentario, post_id, fechaEscritura,idusuario) values ('"+comentarios.get(i)+"','"+i+"',null,(select top(1) id from Usuario where usuario='"+usuario.get(i)+"'))";
            b.modificarBBDD("Insert into Categoria (nombre) values('"+categorias.get(i)+"')");
            b.modificarBBDD("Insert into Usuario (usuario) values ('"+usuario.get(i)+"') ");
            b.modificarBBDD(insertar);
            b.modificarBBDD(insertarCom);
            //b.modificarBBDD("Insert into Post (titulo,fechaCreacion,contenido,categoria_id,idUsuario) values('"+titulos.get(i)+"','"+fechasPost.get(i)+"','"+contenido.get(i)+"',(select id from categoria where nombre='"+categorias.get(i)+"'),(select top(1) id from Usuario where usuario='"+usuario.get(i)+"'))");
           // b.modificarBBDD("Insert into Comentario (comentario, post_id, fechaEscritura,idusuario) values ('"+comentarios.get(i)+"','"+i+"',null,'(select id from Usuario where usuario='"+usuario.get(i)+"'))");
            
            
        }     
}

public static void  mostrarUnPost (bbdd c){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el numero de Post del titulo que quieres ver");
    int id=sc.nextInt();
    c.leerBBDD("select titulo from Post where id="+id+"",1);


    
}


public static void  mostrarTodosLosPost(bbdd b){
    b.leerBBDD("select * from Post",5);
}


public static void mostrarPosrtID(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el numero de Post que quieres visualizar");
    int id=sc.nextInt();
    b.leerBBDD("select * from Post where id="+id+"",5);
}


public static void mostrarPorCategoria(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el numero de Categoria que quieres visualizar");
    int idCat=sc.nextInt();
    b.leerBBDD("select * from Post where categoria_id="+idCat+"",5);
}

public static void mostrarTituloContenido(bbdd b){
    Scanner sc=new Scanner(System.in);
    int opcion;

    do {																					
                System.out.println("|ººººººººººººººººººººººººººººººººººººººººººººº|");				
                System.out.println("|1. Buscar por titulo                         |");				
                System.out.println("|2. Buscar por contenido                      |");
                System.out.println("|3. Salir                                     |");
                System.out.println("|_____________________________________________|");
        opcion= sc.nextInt();

         switch (opcion) {
            case 1:
                buscarTitulo(b);
                break;
    
            case 2:
                buscarContenido(b);
                break;
        }
        
    }while (opcion != 3);
}
public static void buscarTitulo(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el titulo que deseas buscar");
    String titulo=sc.nextLine();
    b.leerBBDD("select titulo from Post where titulo like '%"+titulo+"%'",1);
}
public static void buscarContenido(bbdd b){
    Scanner sc=new Scanner(System.in);
    System.out.println("Introduce el Contenido que deseas buscar"); 
    String contenido=sc.nextLine();
    b.leerBBDD("select titulo from Post where contenido like '%"+contenido+"%'",1); 
}

private static void CrearBBDD()
    {
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
       
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname);
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBBDD.sql");
          
      
        }
    }
    public static void CreatTabla(){
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        String Categorias= b.leerBBDDUnDato("SELECT count(1) FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA = 'dbo' and TABLE_NAME='Pategorias' ");
        String Post= b.leerBBDDUnDato("SELECT count(1) FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA = 'dbo' and TABLE_NAME='Post'");
        String Usuario= b.leerBBDDUnDato("SELECT count(1) FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA = 'dbo' and TABLE_NAME='Usuario'");
        String Comentario= b.leerBBDDUnDato("SELECT count(1) FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA = 'dbo' and TABLE_NAME='Comentario'");


        if (Categorias.equals("0")){
            b.modificarBBDD("create table Categoria( id int not null IDENTITY(1,1) primary key, nombre varchar(255));");   
        } 
        if(Post.equals("0")){
            b.modificarBBDD("create table Post( id int not null IDENTITY(1,1) primary key, titulo varchar(255), fechaCreacion datetime, categoria_id int not null, idUsuario int not null, CONSTRAINT FK_Categoria_id foreign key (categoria_id) references Categoria(id), CONSTRAINT FK_idUsuarioP foreign key (idUsuario) references Usuario(id)");

        }  
        if(Usuario.equals("0")){
            b.modificarBBDD("create table Usuario(id int not null IDENTITY(1,1) primary key, nombre varchar(50),apellido varchar(50), usuario varchar(50), email varchar(255)pass varchar(60));");

        }
        if(Comentario.equals("0")){
            b.modificarBBDD("create table Comentario( id int not null IDENTITY(1,1) primary key, nombre varchar(255), comentario varchar(255), email varchar(255), post_id int not null, fechaEscritura datetime, idUsuario int not null,CONSTRAINT FK_idUsuario foreign key (idUsuario) references Usuario(id)");
          
        }


    }
    
  
    
    
    
    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret =ret +cadena;
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }
    
}
