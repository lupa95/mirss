create table Categoria(
	id int not null IDENTITY(1,1) primary key,
	nombre varchar(255)
);
create table Usuario(
	id int not null IDENTITY(1,1) primary key,
	usuario varchar(50),
);

create table Post(
	id int not null IDENTITY(1,1) primary key,
	titulo varchar(255),
	fechaCreacion nvarchar(255),
	contenido varchar(max),
	categoria_id int not null,
	idUsuario int not null,
	CONSTRAINT FK_Categoria_id foreign key (categoria_id) references Categoria(id),
	CONSTRAINT FK_idUsuarioP foreign key (idUsuario) references Usuario(id),
);


create table Comentario(
	id int not null IDENTITY(1,1) primary key,
	
	comentario varchar(255),
	
	post_id int not null,
	fechaEscritura nvarchar(255),
	idUsuario int not null,
	CONSTRAINT FK_Post_id foreign key (post_id) references post(id),
    CONSTRAINT FK_idUsuario foreign key (idUsuario) references Usuario(id),
);